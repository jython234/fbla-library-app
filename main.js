"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const electron = require("electron");
const path = require("path");
const url = require("url");
const app = electron.app;
let libraryApp;
// NOTE: Base electron code used from https://github.com/electron/electron-quick-start/blob/master/main.js
// Create our app once Electron is ready
app.on("ready", () => {
    libraryApp = new LibraryElectronApp(__dirname + "/index.html");
});
// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
});
app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (libraryApp.hasClosed === null) {
        libraryApp = new LibraryElectronApp(path.join(__dirname, "index.html"));
    }
});
class LibraryElectronApp {
    get hasClosed() { return this.mainWindow == null ? true : false; }
    constructor(pathToIndex) {
        this.mainWindow = new electron.BrowserWindow({
            title: "Newcrest High School Library",
            width: 1200,
            height: 700
        });
        this.mainWindow.loadURL(url.format({
            pathname: pathToIndex,
            protocol: 'file:',
            slashes: true
        }));
        this.mainWindow.on('closed', this.onClose.bind(this));
        this.mainWindow.maximize();
        //this.mainWindow.webContents.openDevTools();
    }
    onClose() {
        this.mainWindow = null;
    }
}
