const library = require("./js/api/library.js");

console.log(__dirname);
console.log(library.libraryUUID);

let fblaLibraryApp = angular.module('fblaLibraryApp', ["ngRoute"]);

// appServices.js
registerServices(fblaLibraryApp, library);

fblaLibraryApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "login.html",
            requireLogin : false,
        })
        .when("/panel", {
            templateUrl : "panel.html",
            requireLogin : true
        });
}]).run( function ($rootScope, $location, $timeout, $routeParams, $anchorScroll, LoginService) {
    $rootScope.changeView = function(view) {
        $location.path(view); // path not hash
    };

    $rootScope.scrollTo = function(id) {
        var old = $location.hash();
        $location.hash(id);
        $anchorScroll();
        //reset to old to keep any additional routing logic from kicking in
        $location.hash(old);
    };

    $rootScope.$on('$routeChangeStart', function (event, next) {
        if(next.requireLogin && !LoginService.getUserLoggedIn()) {
            console.log("No access to panel: not logged in.");

            alert("You need to be logged in to see this page!");
            event.preventDefault();

            $timeout(function () {
                $rootScope.changeView("/");
            }, 500);
        }
    });
});

fblaLibraryApp.controller('exitController', function exitController($scope, $window, $rootScope, LoginService, LibraryService) {
    $scope.onExit = function() {
        // TODO: close database and everything
        //LoginService.setUserLoggedIn(false);
        //$rootScope.changeView("/");
    };

    $window.onbeforeunload = $scope.onExit;
});

fblaLibraryApp.controller("LoginController", function LoginController($scope, $rootScope, $window, LoginService, LibraryService) {
    $scope.init = function() {
        $("#successAlert").hide();
        $("#failureAlert").hide();

        if(LoginService.getUserLoggedIn()) {
            // User is already logged in
            $rootScope.changeView("/panel");
        }

        LibraryService.setupDatabase();
    }

    $scope.doLogin = function(login) {
        console.log("Yeah, someone pressed that login button.");
        if(login == undefined || login.user == undefined || login.password == undefined || login.user == "" || login.password == "") {
            document.getElementById("failureAlertText").innerHTML = "Please enter a valid username and password.";
            $("#failureAlert").show();
        } else {
            console.log("Processing");
            LoginService.doLogin(login, LoginService, LibraryService, $scope, $rootScope)
        }
    };
});

fblaLibraryApp.controller("PanelController", function PanelController($scope, $rootScope, LoginService, LibraryService) {
    $scope.init = function() {
        console.log(LoginService.user);
        if(LoginService.user.personType == library.PersonType.Teacher || LoginService.user.personType == library.PersonType.Admin) {
            document.getElementById("linkRegisterUser").className = "";
            document.getElementById("linkManageUsers").className = "";
            document.getElementById("linkLibrarySettings").className = "";
            document.getElementById("linkGlobalReports").className = "";
        }

        LibraryService.fetchLibrarySettings(LibraryService, $scope);

        let checkedOutBooks = [];
        for(var i = 0; i < LoginService.user.checkedOutBooks.length; i++) {
            let book = LoginService.user.checkedOutBooks[i];
            LibraryService.getBook(book, LibraryService, (bookData) => {
                let checkedOutAt = new Date(bookData.checkedOutAt);
                let dueDate = new Date(bookData.checkedOutAt);
                dueDate.setDate(dueDate.getDate() + 14);

                checkedOutBooks.push({
                    name: bookData.name,
                    author: bookData.author,
                    _id: bookData._id,
                    checkedOutAt: checkedOutAt.toLocaleDateString("en-US"),
                    dueDate: dueDate.toLocaleDateString("en-US")
                });

                $scope.$apply(function() {
                    $scope.checkedOutBooks = checkedOutBooks;
                });
            });
        }
    };

    $scope.triggerRegisterUser = function() {
        if(LoginService.user.personType == library.PersonType.Teacher || LoginService.user.personType == library.PersonType.Admin) {
            $("#registerUserModal").modal("show");
            $("#registerFailureAlert").hide();
        }
    };

    $scope.registerUser = function(registerUserData) {
        console.log(registerUserData);

        if(registerUserData == undefined || registerUserData.user == undefined || registerUserData.user == ""
            || registerUserData.fullname == undefined || registerUserData.fullname == ""
            || registerUserData.password == undefined || registerUserData.password == ""
            || registerUserData.accountType == undefined || registerUserData.accountType == "") {

            document.getElementById("registerFailureAlertText").innerHTML = "Please enter a valid Username, fullname and password.";
            $("#registerFailureAlert").show();
        } else {
            LibraryService.registerUser(registerUserData.user, registerUserData.fullname, registerUserData.password, registerUserData.accountType, LibraryService, $scope);
        }
    }
;
    $scope.triggerLibrarySettings = function() {
        $("#librarySettingsModal").modal("show");
        $("#librarySettingsFailureAlert").hide();
    };

    $scope.updateLibrarySettings = function(librarySettings) {
        LibraryService.updateLibrarySettings(LibraryService, librarySettings, $scope, () => {
            $("#librarySettingsFailureAlert").hide();
            $("#librarySettingsModal").modal("hide");
        });
    };

    $scope.triggerCheckoutBook = function() {
        $("#checkoutBookModal").modal("show");
        $("#checkoutBookFailureAlert").hide();
    }

    $scope.searchBook = function(title) {
        console.log("Searching for: " + title);
        LibraryService.getLibraryInstance().searchBooksByTitle(title).then((results) => {
            let foundBooks = [];
            $scope.$apply(function () {
                $scope.foundBooks = [];
            });

            for(let i = 0; i < results.length; i++) {
                console.log(results[i]);
                LibraryService.getBook(results[i].id, LibraryService, (bookData) => {
                    let dueDate = new Date(bookData.checkedOutAt);
                    dueDate.setDate(dueDate.getDate() + 14);

                    let isCheckedOut = (bookData.checkedOutTo == "" ? false : true);

                    foundBooks.push({
                        name: bookData.name,
                        author: bookData.author,
                        _id: bookData._id,
                        isCheckedOut: isCheckedOut,
                        dueDate: isCheckedOut ? dueDate : "N/A",
                        buttonClass: isCheckedOut ? "btn btn-warning btn-fab-mini disabled" : "btn btn-success btn-fab-mini"
                    });

                    $scope.$apply(function() {
                        $scope.foundBooks = foundBooks;
                    });
                });
            }

            $("#modalSearchBooksTable").show();
        }).catch((err) => {
            console.error(err)

        });
    };

    $scope.checkoutBook = function(uuid) {
        console.log("Attempting to checkout book: " + uuid + ", for: " + LoginService.user._id);
        LibraryService.getLibraryInstance().checkoutBookByUUID(LoginService.user._id, uuid).then((bookId) => {
            console.log("Checked out book: " + bookId);

            $("#checkoutBookFailureAlert").hide();
            document.getElementById("formCheckoutBook").reset();
            $("#checkoutBookModal").modal("hide");

            LibraryService.getBook(bookId, LibraryService, (bookData) => {
                let checkedOutAt = new Date(bookData.checkedOutAt);
                let dueDate = new Date(bookData.checkedOutAt);
                dueDate.setDate(dueDate.getDate() + 14);

                $scope.checkedOutBooks.push({
                    name: bookData.name,
                    author: bookData.author,
                    _id: bookData._id,
                    checkedOutAt: checkedOutAt.toLocaleDateString("en-US"),
                    dueDate: dueDate.toLocaleDateString("en-US")
                });

                $scope.$apply(function() {
                    $scope.foundBooks = [];
                    $scope.checkedOutBooks = $scope.checkedOutBooks;
                });
            });

            alert("Success!");
        }).catch((err) => {
            document.getElementById("checkoutBookFailureAlertText").innerHTML = err.message;
            $("#checkoutBookFailureAlert").show();
        });
    }

    $scope.returnBook = function(bookId) {
        LibraryService.getLibraryInstance().returnBook(LoginService.user._id, bookId).then(() => {
            // Success, let's remove the book from the Array
            let newBooks = [];
            for(book of $scope.checkedOutBooks) {
                if(book._id != bookId) {
                    newBooks.push(book);
                }
            }

            $scope.$apply(function() {
                $scope.checkedOutBooks = newBooks;
            });
        }).catch((err) => console.error(err));
    }

    $scope.doLogout = function() {
        console.log("Logging out");
        LoginService.doLogout(LoginService, $rootScope);
    }
});
