function registerServices(fblaLibraryApp, library) {
    fblaLibraryApp.service("LoginService", function ($http, $timeout) {
        this.setUserLoggedIn = function(value){
           localStorage.userIsLoggedIn = value;
        };

        this.getUserLoggedIn = function() {
           return localStorage.userIsLoggedIn == 'true';
        };

        this.user = null;

        this.doLogin = function(login, LoginService, LibraryService, $scope, $rootScope) {
            let self = this;

            LibraryService.getLibraryInstance().verifyLogin(login.user, login.password).then((verified) => {
               if(verified) {
                   LoginService.setUserLoggedIn(true);

                   LibraryService.getLibraryInstance().getPerson(login.user).then((person) => {
                       self.user = person;
                   });

                   $("#failureAlert").hide();

                   $timeout(function() {
                       $rootScope.changeView("panel");
                   }, 200);
               } else {
                   document.getElementById("failureAlertText").innerHTML = "That username or password is incorrect.";
                   $("#failureAlert").show();
               }
            }).catch((err) => {
               document.getElementById("failureAlertText").innerHTML = err.message;
               $("#failureAlert").show();
            });
       };

       this.doLogout = function(LoginService, $rootScope) {
           if(LoginService.getUserLoggedIn() === false) return;

           LoginService.setUserLoggedIn(false);
           $("#failureAlert").hide();

           $timeout(function() {
               $rootScope.changeView("/");
           });
       };
    });

    fblaLibraryApp.service("LibraryService", function($http, $timeout) {
        this.getLibraryInstance = function() {
            return this.database;
        };

        this.setupDatabase = function() {
            if(this.database == null) {
                this.database = new library.Library("library-db");
                console.log("Setup database");
            }
        };

        this.registerUser = function(username, fullname, password, accountTypeStr, LibraryService, $scope) {
            let accountType;
            switch(accountTypeStr) {
                case "Student":
                    accountType = library.PersonType.Student;
                    break;
                case "Teacher":
                    accountType = library.PersonType.Teacher;
                    break;
            }

            LibraryService.getLibraryInstance().registerPerson(fullname, username, password, accountType).then((usrname) => {
                $("#registerUserModal").modal("hide");
                document.getElementById("formRegisterUser").reset();
                $scope.registerUserData = undefined;
                alert("Success!");
                // Success
            }).catch((err) => {
                document.getElementById("registerFailureAlertText").innerHTML = err.message;
                $("#registerFailureAlert").show();
            });
        };

        this.getBook = function(uuid, LibraryService, callback) {
            LibraryService.getLibraryInstance().getBook(uuid).then((book) => {
                callback(book);
            }).catch((err) => console.error(err));
        };

        this.fetchLibrarySettings = function(LibraryService, $scope) {
            LibraryService.getLibraryInstance().getMainDbObject().then((obj) => {
                $scope.$apply(function() {
                    $scope.librarySettings = obj;
                    $scope.librarySettingsModel = obj;
                });
            }).catch((err) => console.error(err));
        };

        this.updateLibrarySettings = function(LibraryService, librarySettings, $scope, cb) {
            LibraryService.getLibraryInstance().updateBookLimit(librarySettings.bookLimitStudent, librarySettings.bookLimitTeacher).then(() => {
                LibraryService.getLibraryInstance().updateBookTimeLimit(librarySettings.bookTimeLimitStudent, librarySettings.bookTimeLimitTeacher).then((obj) => {
                    $scope.$apply(function() {
                        $scope.librarySettings = obj;
                        $scope.librarySettingsModel = obj;
                    });
                    cb();
                }).catch((err) => console.error(err));
            }).catch((err) => console.error(err));
        };
    });
 }
