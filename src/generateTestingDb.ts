import { Library, PersonType } from "./api/library";

let library = new Library("library-db");
library.registerPerson("John Smith", "jsmith", "password", PersonType.Teacher).then((username) => {
    console.log("Successfully registered with username: " + username);
}).catch((err) => {
    console.error("Failed to register! " + err);
});

library.registerPerson("Brianna Campbell", "bcampbell", "password2", PersonType.Student).then((username) => {
    console.log("Successfully registered with username: " + username);
}).catch((err) => {
    console.error("Failed to register! " + err);
});

library.addBook("A Wrinkle in Time", "Madeleine L'Engle").then((result) => {
    console.log("Successfully registered book with UUID: " + result)
    library.addBook("A Wrinkle in Time", "Madeleine L'Engle").then((result) => {
        console.log("Successfully registered book with UUID: " + result)
    });
});

library.addBook("Dune", "Frank Herbert").then((result) => {
    console.log("Successfully registered book with UUID: " + result);
})

library.addBook("Mastering TypeScript", "Nathan Rozentals").then((result) => {
    console.log("Successfully registered book with UUID: " + result);
})

library.addBook("Paper Towns", "John Green").then((result) => {
    console.log("Successfully registered book with UUID: " + result);
})
