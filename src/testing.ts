import { Library, PersonType } from "./api/library";
import { v4 } from "uuid";
var v5 = require("uuid/v5");

let library = new Library("library-db");
library.getPerson("alejzeis").then((result) => {
    console.log(result);
    console.log("Deleting");
    library.returnBook("alejzeis", "ea4a321f-4db7-40ab-b64a-3d1e24cc3d8c").then(() => {
        library.getPerson("alejzeis").then((result) => {
            console.log("Checked out books: " + result.checkedOutBooks);
            library.deletePerson("alejzeis").then(() => {
                console.log("Successfully deleted!");
            }).catch((err) => {
                console.log("Failed to delte! " + err);
            });
        });
    });
}).catch((err) => {
    library.registerPerson("Alejandro Zeise", "alejzeis", "thisispassword", PersonType.Admin).then((username) => {
        console.log("Successfully registered with username: " + username);
        library.checkoutBook(username, "Book of all books (version 2)").then((result) => {
            console.log(result);
            library.checkoutBook(username, "Book of all books").then((result) => {
                console.log(result);
            });
        }).catch((err) => {
            console.log("There was an error checking out the book! " + err);
        });
    }).catch((err) => {
        console.error("Failed to register! " + err);
    });
});
