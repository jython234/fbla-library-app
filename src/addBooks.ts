import { Library } from "./api/library";

let library = new Library("library-db");
library.addBook("Book of all books (version 2)", "bookman").then((result) => {
    console.log("Successfully registered book with UUID: " + result)
    library.addBook("Book of all books", "bookman").then((result) => {
        console.log("Successfully registered book with UUID: " + result)
        library.addBook("Book of all books", "bookman").then((result) => {
            console.log("Successfully registered book with UUID: " + result)
        });
    });
});
