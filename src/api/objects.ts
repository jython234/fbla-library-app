import { PersonType } from "./library";

export interface MainObject {
    _id: string;
    _rev: string;

    // Amount of books that can be checked out for someone at a single time
    bookLimitStudent: number;
    bookLimitTeacher: number;

    // Duration for a book being checked out before it is due (in days)
    bookTimeLimitStudent: number;
    bookTimeLimitTeacher: number;

    // Amount to be fined per day per book overdue (in USD)
    bookFineStudent: number;
    bookFineTeacher: number;
}

export interface DbObject {
    _id: string;
    _rev: string;
    type: number;
    name: string;
}

export interface Person extends DbObject {
    personType: PersonType;
    // Password is stored as a SHA256 hash
    password: string;
    checkedOutBooks: Array<string>;
}

export interface Book extends DbObject {
    author: string;
    checkedOutTo: string;
    checkedOutAt: number;
}
