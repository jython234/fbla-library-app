import * as PouchDB from "pouchdb";
import * as PouchDBFind from "pouchdb-find";
import * as PouchDBQuickSearch from "pouchdb-quick-search";
import { v4 } from "uuid";

import { exit } from "process";
import { mkdirSync } from "fs";
import { createHash, Hash } from "crypto";

import { MainObject, DbObject, Person, Book } from "./objects";

// Use pouchdb-find module
PouchDB.plugin(PouchDBFind);
PouchDB.plugin(PouchDBQuickSearch);

// Need to import UUID v5 by RequireJS due to obscure TypeScript error, possibly due to definitions
const v5 = require("uuid/v5");

// Static UUID which is used to generate UUIDs for People
export const libraryUUID = "af587859-f6c1-557c-abb2-fefcf66c4600";

const OBJECT_TYPE_PERSON = 0;
const OBJECT_TYPE_BOOK = 1;

// Since the PersonType is stored as an integer (number) in the database
//  we need a function to create a PersonType instance from that number for us.
export function personTypeFromInt(typeInt: number): PersonType {
    switch(typeInt) {
        case 0:
            return PersonType.Student;
        case 1:
            return PersonType.Teacher;
        case 2:
            return PersonType.Admin;
        default:
            throw new Error("Unknown PersonType");
    }
}

// Represents the type of a Person, such as a Student, Teacher, or Administrator
export enum PersonType {
    Student,
    Teacher,
    Admin,
}

export class Library {
    private database: PouchDB.Database;

    constructor(dbName: string = "library-database") {
        this.database = new PouchDB(dbName);

        this.getMainDbObject().catch((err) => {}); // Run this to generate and add default document if it's not found

        this.database.createIndex({
            index: {
                fields: ['name']
            }
        });
    }

    verifyLogin(username: string, password: string): Promise<boolean> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            // Check if the person exists
            self.objectExists(username).then((meta: {exists: boolean, doc: any}) => {
                if(!meta.exists) {
                    // Person not found, invalid
                    reject(new Error("No person found with that username!"));
                } else {
                    // Let's check their password

                    let hash = createHash("sha256");
                    hash.update(password);
                    let hashed = hash.digest("base64");
                    if(hashed === (meta.doc as Person).password) {
                        resolve(true);
                    } else resolve(false);
                }
            }).catch((err) => reject(err));
        });
    }

    registerPerson(name: string, username: string, password: string, type: PersonType): Promise<string> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            // Check if the person exists
            self.objectExists(username).then((meta: {exists: boolean, doc: any}) => {
                if(meta.exists) {
                    // Person was found, reject
                    reject(new Error("This person is already registered!"));
                } else {
                    // Person does not exist. Let's register them

                    let hash = createHash("sha256");
                    hash.update(password);

                    self.database.put({
                        _id: username,
                        type: OBJECT_TYPE_PERSON,
                        name: name,
                        personType: type,
                        password: hash.digest("base64"),
                        checkedOutBooks: []
                    })
                    .then(() => resolve(username))
                    .catch((err) => reject(err));
                }
            }).catch((err) => reject(err));
        });
    }

    deletePerson(username: string): Promise<any> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            // Check if the person exists
            self.getPerson(username).then((doc) => {
                // Person does exist, delete them

                // TODO: Check for fines

                // Checks if they have any checked out books, and un-checks them out
                if(doc.checkedOutBooks.length > 0) {

                    for(let bookId of doc.checkedOutBooks) {
                        self.getBook(bookId).then((book) => {
                            // Reset this book to be un-checked out (let's say the person returned all their books before deleting their account)
                            book.checkedOutAt = -1;
                            book.checkedOutTo = "";
                            self.database.put(book);
                        });
                    }
                }

                self.database.remove(doc)
                    .then(() => resolve())
                    .catch((err) => reject(err));
            }).catch((err) => {
                if(err.status == 404) {
                    // 404 NOT FOUND, person does not exist. reject
                    reject(new Error("That person was not found, can't delete non-existent person"));
                } else reject(err);
            });
        });
    }

    addBook(name: string, author: string): Promise<string> {
        // Generate a random UUID for the book
        let uuid = v4();

        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            // Check if the book exists
            self.objectExists(uuid).then((meta: {exists: boolean, doc: any}) => {
                if(meta.exists) {
                    // Book was found, reject
                    reject(new Error("This book is already registered!"));
                } else {
                    // Book does not exist. Let's register it

                    self.database.put({
                        _id: uuid,
                        type: OBJECT_TYPE_BOOK,
                        name: name,
                        author: author,
                        checkedOutTo: "",
                        checkedOutAt: -1
                    })
                    .then(() => resolve(uuid))
                    .catch((err) => reject(err));
                }
            }).catch((err) => reject(err));
        });
    }

    searchBooksByTitle(titleSearchString: string): Promise<string[]> {
        let self = this;

        return new Promise((resolve, reject) => {
            let db = self.database as any;
            db.search({
                query: titleSearchString,
                fields: ['name']
            }).then((results) => {
                resolve(results.rows);
            }).catch((err) => reject(err));
        });
    }

    checkoutBook(personUsername: string, bookName: string): Promise<string> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            self.objectExists(personUsername).then((meta: {exists: boolean, doc: any}) => {
                if(!meta.exists || meta.doc.type !== OBJECT_TYPE_PERSON) {
                    // Person does not exist. reject
                    reject(new Error("That person was not found"));
                }

                // Find a book with that name, as there may be multiple copies in the library
                self.database.find({
                    selector: {
                        name: bookName
                    }
                }).then((result) => {
                    if(result.docs.length < 1) {
                        reject(new Error("Could not find a book with that title, maybe it doesn't exist or is misspelled?"));
                        return;
                    }

                    let found = false;
                    for(let element of result.docs) {
                        // Need to cast element to Book in order to access the values of the Document.
                        let doc = element as Book;
                        if(doc.checkedOutTo == "" || doc.checkedOutTo == null) {
                            // This book is not checked out, so let's check it out to this person!
                            self.checkoutBookByUUID(personUsername, doc._id).then((result) => {
                                resolve(result);
                            }).catch((err) => reject(err));
                            found = true;
                            break;
                        } else console.log("Found : " + doc);
                    }

                    if(!found) reject(new Error("Couldn't find a copy of that book that wasn't checked out!"));
                });
            }).catch((err) => reject(err));
        });
    }

    checkoutBookByUUID(personUsername: string, bookUUID: string): Promise<string> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            self.objectExists(personUsername).then((meta: {exists: boolean, doc: DbObject}) => {
                if(!meta.exists || meta.doc.type !== OBJECT_TYPE_PERSON) {
                    // Person does not exist. reject
                    reject(new Error("That person was not found"));
                }

                let person = meta.doc as Person;

                self.getMainDbObject().then((obj) => {
                    // Check if they go over the book amount limit
                    if((person.checkedOutBooks.length + 1) > (person.type == PersonType.Student ? obj.bookLimitStudent : obj.bookLimitTeacher)) {
                        reject(new Error("You've reached the maximum amount of books you can have checked out at once, sorry!"));
                        return;
                    }

                    self.objectExists(bookUUID).then((meta: {exists: boolean, doc: any}) => {
                        if(!meta.exists || meta.doc.type !== OBJECT_TYPE_BOOK) {
                            // Book does not exist, reject
                            reject(new Error("That book was not found"));
                            return;
                        }
                        let book = meta.doc as Book;

                        if(book.checkedOutTo !== "") {
                            reject(new Error("This book is already checked out!"));
                            return;
                        }

                        meta.doc.checkedOutTo = personUsername;
                        meta.doc.checkedOutAt = new Date().getTime();

                        self.database.put(meta.doc);

                        self.getPerson(personUsername).then((person: Person) => {
                            person.checkedOutBooks.push(bookUUID);
                            self.database.put(person);

                            resolve(book._id);
                        }).catch((err) => reject(err));
                    }).catch((err) => reject(err));
                });
            }).catch((err) => reject(err));
        });
    }

    returnBook(personUsername: string, bookUUID: string): Promise<any> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            self.objectExists(personUsername).then((meta: {exists: boolean, doc: DbObject}) => {
                if(!meta.exists || meta.doc.type !== OBJECT_TYPE_PERSON) {
                    // Person does not exist. reject
                    reject(new Error("That person was not found"));
                }

                // Check if the person has that book checked out
                let person = meta.doc as Person;
                if(!person.checkedOutBooks.includes(bookUUID)) {
                    reject(new Error("You don't have that book checked out!"));
                    return;
                }

                // Create a new Array containing all the checked out books, minus the one that we are returning.
                // We have to do this because there is no Array.prototype.remove() method in JavaScript
                let newBooks = new Array<string>();
                person.checkedOutBooks.forEach((element) => { if(element !== bookUUID) newBooks.push(element) } );
                person.checkedOutBooks = newBooks;

                self.database.put(person);

                self.objectExists(bookUUID).then((meta: {exists: boolean, doc: any}) => {
                    if(!meta.exists || meta.doc.type !== OBJECT_TYPE_BOOK) {
                        // Book does not exist, reject
                        reject(new Error("That book was not found"));
                        return;
                    }
                    let book = meta.doc as Book;

                    meta.doc.checkedOutTo = "";
                    meta.doc.checkedOutAt = -1;

                    self.database.put(meta.doc);

                    resolve();
                }).catch((err) => reject(err));
            }).catch((err) => reject(err));
        });
    }

    getObject(uuid: string): Promise<DbObject> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            self.database.get(uuid).then((doc: DbObject) => {
                resolve(doc);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    getPerson(username: string): Promise<Person> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            self.getObject(username).then((doc: DbObject) => {
                // Check if the object is a Person or not
                if(doc.type !== OBJECT_TYPE_PERSON) {
                    reject(new Error("This UUID belongs to a book!"));
                } else resolve(doc as Person);
            }).catch((err) => reject(err));
        });
    }

    getBook(uuid: string): Promise<Book> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            self.getObject(uuid).then((doc: DbObject) => {
                // Check if the object is a Book or not
                if(doc.type !== OBJECT_TYPE_BOOK) {
                    reject(new Error("This UUID belongs to a person!"));
                } else resolve(doc as Book);
            }).catch((err) => reject(err));
        });
    }

    getMainDbObject(): Promise<MainObject> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            this.database.get("!main__").catch((err) => {
                if(err.status == 404) {
                    // NOT_FOUND, let's create and put the default values
                    let obj = {
                        _id: "!main__",
                        bookLimitStudent: 10,
                        bookLimitTeacher: 20,
                        bookTimeLimitStudent: 14,
                        bookTimeLimitTeacher: 21,
                        bookFineStudent: 0.1,
                        bookFineTeacher: 0.05
                    };

                    self.database.put(obj);
                    return obj;
                } else {
                    throw err;
                }
            }).then((doc) => {
                resolve(doc as MainObject);
            }).catch((err) => {
                console.error("There was an error when trying to load the main database object!");
                console.error(err);
                exit(1);
            })
        });
    }

    updateBookLimit(amountStudent: number = 10, amountTeacher: number = 20): Promise<MainObject> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            this.database.get("!main__").then((doc: MainObject) => {
                // Update values
                doc.bookLimitStudent = amountStudent;
                doc.bookLimitTeacher = amountTeacher;

                // Re-insert into database
                self.database.put(doc);
                resolve(doc);
            }).catch((err) => reject(err));
        });
    }

    updateBookTimeLimit(limitStudent: number = 14, limitTeacher: number = 21): Promise<MainObject> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            this.database.get("!main__").then((doc: MainObject) => {
                // Update values
                doc.bookTimeLimitStudent = limitStudent;
                doc.bookTimeLimitTeacher = limitTeacher;

                // Re-insert into database
                self.database.put(doc);
                resolve(doc);
            }).catch((err) => reject(err));
        });
    }

    objectExists(id: string): Promise<{exists: boolean, doc: DbObject}> {
        // Need to assign current instance to variable, as "this" in the Promise functions will refer to the Promise, not the Library
        let self = this;

        return new Promise((resolve, reject) => {
            self.getObject(id).then((doc: DbObject) => {
                // Object does exist
                resolve({exists: true, doc: doc});
            }).catch((err) => {
                if(err.status == 404) {
                    // 404 NOT FOUND, object does not exist
                    resolve({exists: false, doc: null});
                } else reject(err);
            });
        });
    }

    lookupUUIDByName(name: string): string {
        return v5(name, libraryUUID);
    }
}
